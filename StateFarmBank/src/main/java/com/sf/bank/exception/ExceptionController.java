package com.sf.bank.exception;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
public class ExceptionController {
	
	
	@ExceptionHandler(MyException.class)
	public ModelAndView handleMyException(MyException ex)
	{
		ModelAndView mav = new ModelAndView();
		mav.addObject("errMsg", ex.getMessage());
		mav.setViewName("error");
		return mav;
	}
	
	
	  @ExceptionHandler(Exception.class)
	  public ModelAndView handleException(Exception ex) 
	  {
		  ModelAndView mavObj = new ModelAndView();
		  mavObj.addObject("errMsg", ex.getMessage());
		  mavObj.setViewName("error");
		  return mavObj; 
      }
	 

}
