package com.sf.bank.bean;

public class DebitCardDetails {
	
	protected long debitCardNo;
	protected String vaildUpTo;
	protected int cvv;
	protected int accountOTP;
	
	
	public DebitCardDetails() {
		super();
		// TODO Auto-generated constructor stub
	}


	public DebitCardDetails(long debitCardNo, String vaildUpTo, int cvv, int accountOTP) {
		super();
		this.debitCardNo = debitCardNo;
		this.vaildUpTo = vaildUpTo;
		this.cvv = cvv;
		this.accountOTP = accountOTP;
	}


	public long getDebitCardNo() {
		return debitCardNo;
	}


	public void setDebitCardNo(long debitCardNo) {
		this.debitCardNo = debitCardNo;
	}


	public String getVaildUpTo() {
		return vaildUpTo;
	}


	public void setVaildUpTo(String vaildUpTo) {
		this.vaildUpTo = vaildUpTo;
	}


	public int getCvv() {
		return cvv;
	}


	public void setCvv(int cvv) {
		this.cvv = cvv;
	}


	public int getAccountOTP() {
		return accountOTP;
	}


	public void setAccountOTP(int accountOTP) {
		this.accountOTP = accountOTP;
	}
	
	
	

}
