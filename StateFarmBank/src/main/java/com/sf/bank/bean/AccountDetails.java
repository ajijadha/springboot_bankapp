package com.sf.bank.bean;

public class AccountDetails {

	protected int accountBalance;
	protected String accountType;
	protected long accountNo;
	protected long customerId;
	protected Boolean isJointAccount;
	protected int mPIN;
	
	
	
	public AccountDetails() {
		super();
		// TODO Auto-generated constructor stub
	}


	public AccountDetails(int accountBalance, String accountType, long accountNo, long customerId, Boolean isJointAccount, int mPIN) {
		super();
		
		this.accountBalance = accountBalance;
		this.accountType = accountType;
		this.accountNo = accountNo;
		this.customerId = customerId;
		this.isJointAccount = isJointAccount;
		this.mPIN = mPIN;
		
	}


	public int getAccountBalance() {
		return accountBalance;
	}


	public void setAccountBalance(int accountBalance) {
		this.accountBalance = accountBalance;
	}


	public String getAccountType() {
		return accountType;
	}


	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}


	public long getAccountNo() {
		return accountNo;
	}


	public void setAccountNo(long accountNo) {
		this.accountNo = accountNo;
	}


	public long getCustomerId() {
		return customerId;
	}


	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}


	public Boolean getIsJointAccount() {
		return isJointAccount;
	}


	public void setIsJointAccount(Boolean isJointAccount) {
		this.isJointAccount = isJointAccount;
	}


	public int getmPIN() {
		return mPIN;
	}


	public void setmPIN(int mPIN) {
		this.mPIN = mPIN;
	}
	
}
