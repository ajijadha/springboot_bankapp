package com.sf.bank.bean;

public class BankDetails {
	
   protected String bankName;
   protected String branchName;
   protected int ifscCode;
   protected String branchAddress;
   protected Boolean isInternational;
   protected long branchContact;
   protected int branchCode;
   
   public BankDetails() {
	super();
	// TODO Auto-generated constructor stub
}

public BankDetails(String bankName, String branchName, int ifscCode, String branchAddress, Boolean isInternational,
		long branchContact, int branchCode) {
	super();
	this.bankName = bankName;
	this.branchName = branchName;
	this.ifscCode = ifscCode;
	this.branchAddress = branchAddress;
	this.isInternational = isInternational;
	this.branchContact = branchContact;
	this.branchCode = branchCode;
}

public String getBankName() {
	return bankName;
}

public void setBankName(String bankName) {
	this.bankName = bankName;
}

public String getBranchName() {
	return branchName;
}

public void setBranchName(String branchName) {
	this.branchName = branchName;
}

public int getIfscCode() {
	return ifscCode;
}

public void setIfscCode(int ifscCode) {
	this.ifscCode = ifscCode;
}

public String getBranchAddress() {
	return branchAddress;
}

public void setBranchAddress(String branchAddress) {
	this.branchAddress = branchAddress;
}

public Boolean getIsInternational() {
	return isInternational;
}

public void setIsInternational(Boolean isInternational) {
	this.isInternational = isInternational;
}

public long getBranchContact() {
	return branchContact;
}

public void setBranchContact(long branchContact) {
	this.branchContact = branchContact;
}

public int getBranchCode() {
	return branchCode;
}

public void setBranchCode(int branchCode) {
	this.branchCode = branchCode;
}
   
   
	

}
