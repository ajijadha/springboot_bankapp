package com.sf.bank.bean;

import java.util.Date;

public class CustomerDetails {
	

	protected String customerName;
	protected Date dateOfBirth;
	protected String customerAddress;
	protected int contact;
	protected String emailId;
	protected String adharNo;
	protected Boolean isEmployed;
	
	
	public CustomerDetails() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	public CustomerDetails(String customerName, Date dateOfBirth, String customerAddress, int contact,
			String emailId, String adharNo, Boolean isEmployed) {
		super();
		this.customerName = customerName;
		this.dateOfBirth = dateOfBirth;
		this.customerAddress = customerAddress;
		this.contact = contact;
		this.emailId = emailId;
		this.adharNo = adharNo;
		this.isEmployed = isEmployed;
	}
	
	
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getCustomerAddress() {
		return customerAddress;
	}
	public void setCustomerAddress(String customerAddress) {
		this.customerAddress = customerAddress;
	}
	public int getContact() {
		return contact;
	}
	public void setContact(int contact) {
		this.contact = contact;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getAdharNo() {
		return adharNo;
	}
	public void setAdharNo(String adharNo) {
		this.adharNo = adharNo;
	}
	public Boolean getIsEmployed() {
		return isEmployed;
	}
	public void setIsEmployed(Boolean isEmployed) {
		this.isEmployed = isEmployed;
	}
	
	
	
	
	

}
