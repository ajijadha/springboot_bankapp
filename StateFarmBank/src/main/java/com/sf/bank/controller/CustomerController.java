package com.sf.bank.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sf.bank.bean.CustomerDetails;
import com.sf.bank.dao.IBankDAO;
import com.sf.bank.exception.MyException;

@Controller
public class CustomerController {
	
	@Autowired
	private IBankDAO bankDAO;
	
	@RequestMapping(value="/getAllCustomer", method=RequestMethod.GET)
	@ResponseBody
	public List<CustomerDetails> getAllCustRecord()
	{
	
		List<CustomerDetails> allCustomers = bankDAO.getAllCustomers();
		if(allCustomers == null)
		{
			throw new MyException("allCustomers is null");
		}
		return allCustomers;
		
	}
	
}
