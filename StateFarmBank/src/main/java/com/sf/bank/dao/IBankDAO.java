package com.sf.bank.dao;

import java.util.List;

import com.sf.bank.bean.CustomerDetails;

public interface IBankDAO {
	
	public List<CustomerDetails> getAllCustomers();

}
