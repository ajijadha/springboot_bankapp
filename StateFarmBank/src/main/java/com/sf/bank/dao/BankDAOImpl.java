package com.sf.bank.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sf.bank.bean.CustomerDetails;
import com.sf.bank.mapper.CustRowMapper;

@Repository
public class BankDAOImpl implements IBankDAO {
	
	@Autowired
    private JdbcTemplate jdbcTemplate;

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<CustomerDetails> getAllCustomers() {
		return jdbcTemplate.query("select * from CustomerDetails", 
                new CustRowMapper());
	}

}
