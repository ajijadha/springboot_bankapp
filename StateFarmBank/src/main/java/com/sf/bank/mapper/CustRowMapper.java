package com.sf.bank.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.sf.bank.bean.CustomerDetails;

public class CustRowMapper implements RowMapper {

	@Override
	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		 
		CustomerDetails custDetails = new CustomerDetails();
		custDetails.setCustomerName(rs.getString("customerName"));
		custDetails.setDateOfBirth(rs.getDate("dateOfBirth"));
		custDetails.setCustomerAddress(rs.getString("customerAddress"));
		custDetails.setContact(rs.getInt("contact"));
		custDetails.setEmailId(rs.getString("emailId"));
		custDetails.setAdharNo(rs.getString("adharNo"));
		custDetails.setIsEmployed(rs.getBoolean("isEmployed"));
		return custDetails;
	}

}
